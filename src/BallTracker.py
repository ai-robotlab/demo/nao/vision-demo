# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 15:56:34 2016

@author: Luc Nies
"""
import os
import time
from naoqi import ALProxy
import NAOMovement
import cv2
import NAOCamera
import numpy as np
import sys
from IPython import embed
import linecache
import solver


class BallTracker():

	def __init__(self, ip = '169.254.95.24', port = 9559, sit = True, demo = True):

		self.topCamera = NAOCamera.NaoCam(name = 'Top', IP=ip, bottom_camera=0)
		self.botCamera = NAOCamera.NaoCam(name = 'Bot', IP=ip, bottom_camera=1)
		self.focalLength = 140 #calculated
		self.lower_blue = np.array([70, 50, 50], dtype=np.uint8)
		self.upper_blue = np.array([170, 255, 255], dtype=np.uint8)
		self.motion = NAOMovement.NAOMovement(ip=ip, port=port, sit=sit)
		self.memory = ALProxy("ALMemory", ip, port)
		self.sensorsHead = [
			'Device/SubDeviceList/Head/Touch/Front/Sensor/Value',
			'Device/SubDeviceList/Head/Touch/Rear/Sensor/Value',
			'Device/SubDeviceList/Head/Touch/Middle/Sensor/Value'
		]

		self.bot_ball = None
		self.top_ball = None
		self.top_frame = None
		self.bot_frame = None
		self.top_ball_image = None
		self.bot_ball_image = None
		self.topCamera.subscribe_camera()
		self.botCamera.subscribe_camera()

	def demo(self):

		default_pitch = -0.2
		default_yaw = 0
		weights = solver.train_weights()
		self.motion.move_head_to(default_yaw, default_pitch)
		reached = False
		while True:
			#todo counter if lost

			self.bot_frame = self.botCamera.get_frame(self.botCamera.camera)
			self.bot_image, self.bot_ball_image, self.bot_ball = self.detect_ball(self.bot_frame)
			
			cv2.imshow('Search ball bot', self.bot_image)
			cv2.imshow('Search ball bot, ball view',  self.bot_ball_image)

			if self.bot_ball is not None:
				
				if self.centrate_ball(self.bot_image, self.bot_ball, threshold = 5):
					angles = self.motion.get_head_angles()
					distance = self.distanceTo(self.bot_ball)
					np.array(angles.append(distance))
					pos = solver.find_coor(angles, weights)[0]
					print pos
					self.motion.cartesian_move_arm_to(pos)
					reached = True
			
			elif reached:
				#self.motion.motion.rest()
				self.motion.sit()
				reacher = False
					
			if cv2.waitKey(33) == 27:
				self.shut_down()


	def lost_ball(self, n_sweeps = 4, sweeping_factor = 0.2):


		i = 1
		direction = True

		while i-1 < n_sweeps:
			if direction:
				multiplier = i * -1
			else:
				direction
				multiplier = i

			target_yaw, target_pitch = self.motion.move_head(1 * multiplier, 0, factor=sweeping_factor)
			while not self.motion.is_head_on_target(target_yaw, target_pitch) and self.motion.target_in_range(target_yaw, target_pitch):

				self.check_both_cams()

				if self.top_ball is not None or self.bot_ball is not None:
					self.motion.move_head(0, 0)
					return True
			i += 1
			direction = not direction

		return False


	def search_for_ball(self, sweeping_factor= 0.5):
		"""
		:param yaw_step:
		:param pitch_step:
		:return:
		"""

		yaw_limit = {-1:-1.8, 1:1.8}
		pitch_limit = {-1:-0.6720, 0:0, 1:0.5149}
		direction_pitch = -1
		direction_yaw = 1



		self.motion.move_head_to(yaw_limit[direction_yaw], pitch_limit[direction_pitch], 0.25)
		while not self.motion.is_head_on_target(yaw_limit[direction_yaw], pitch_limit[direction_pitch]):
			pass

		direction_yaw = -1
		self.motion.move_head_to(yaw_limit[direction_yaw], pitch_limit[direction_pitch])
		while True:
			curr_yaw, curr_pitch = self.motion.get_head_angles()
			#Switch direction
			if self.motion.is_head_on_target(yaw_limit[direction_yaw], pitch_limit[direction_pitch]):
				direction_yaw *= -1
				direction_pitch += 1
				if direction_pitch < 2:
					self.motion.move_head_to(yaw_limit[direction_yaw], pitch_limit[direction_pitch])
				else:
					self.motion.move_head_to(0, 0, speed=0.5)# Put head in new starting position
					time.sleep(1)
					self.destroy_search_windows()
					return False

			self.check_both_cams()
			cv2.imshow('Search ball top', self.top_image)
			cv2.imshow('Search ball top, ball view', self.top_ball_image)
			cv2.imshow('Search ball bot', self.bot_image)
			cv2.imshow('Search ball bot, ball view',  self.top_ball_image)

			if self.top_ball is not None or self.bot_ball is not None:
				self.motion.move_head(0, 0)
				self.destroy_search_windows()
				return True

			if cv2.waitKey(33) == 27:
				self.shut_down()


	def destroy_search_windows(self):
		cv2.destroyWindow('Search ball top')
		cv2.destroyWindow('Search ball top, ball view')
		cv2.destroyWindow('Search ball bot')
		cv2.destroyWindow('Search ball bot, ball view')

	def shut_down(self):
		cv2.destroyAllWindows()
		self.topCamera.unsubscribe_camera()
		self.botCamera.unsubscribe_camera()
		self.motion.move_head_to(0,0, speed = 0.5)
		time.sleep(0.5)
		self.motion.motion.rest()
		sys.exit(0)

	def detect_ball(self, frame):
		"""

		:param image:
		:return image, ball_image, ball:
		"""
		hsvImage = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

		mask = cv2.inRange(hsvImage, self.lower_blue, self.upper_blue)

		denoised = self.remove_noise(mask)  # perform opening and closing

		smoothed = cv2.GaussianBlur(denoised, (13, 13), 0)  # Smooth edges with gaussian blur
		# only pick the ball
		ball_image = cv2.bitwise_and(frame, frame, mask=smoothed)
		gray = ball_image[:, :, 2]

		circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 1, param1=200, param2=20, minRadius=5, maxRadius=100)  # Find circles, hardcoded parameters, are probably better ways of finding these

		image = np.copy(frame)
		if circles is not None:
			circle = circles[0, :][0]

			# draw circle around ball

			cv2.circle(image, (circle[0], circle[1]), circle[2], (0, 255, 0), 2)  # always picking the first seems to work (as it should). Might consider picking the mean of everything.

			# calc distance to ball
			distance = self.distanceTo(circle)
			distance_text = format(distance, '0.2f')

			cv2.putText(image, distance_text, (int(circle[0] + circle[2]) + 5, int(circle[1] + circle[2] + 5)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
			ball = circle
		else:
			ball = None

		return image, ball_image, ball

	def distance_to_ball(self, arm_movement = True, counter = 0):
		self.camera.subscribe_camera()
		self.motion.setStiffnesses("Head", 1.0)

		self.motion.random_arm_movement()
		while True:

			image = self.camera.get_frame(self.camera.camera)
			image, ball_image, ball = self.detect_ball(image)  # ball_image is for debugging purposes

			if ball is not None:
				image = self.centrate_ball(image, ball)
				#if arm_movement:
					#self.motion.random_arm_movement()

			else:
				if not self.search_for_ball():
					print "no ball found, try next position"
					self.motion.random_arm_movement()#ToDo Blocking call

			#self.motion.monitor_joints()
			cv2.imshow("Distance to ball", image)
			cv2.imshow("Balls cam", ball_image)


			# exit by [ESC]
			if cv2.waitKey(33) == 27:
				self.camera.unsubscribe_camera()
				self.motion.setStiffnesses("Head", 0.0)
				cv2.destroyWindow("Distance to ball")
				break


	def remove_noise(self, image):
		kernel = np.ones((9, 9), np.uint8)
		opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
		closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel)

		return closing


	"""
	circle object
	distance to object in cm
	diameter of object in cm
	"""
	def find_focal_length(self, distance, object_size=5):

		while True:

			self.check_both_cams()

			cv2.imshow('Search ball top', self.top_image)
			cv2.imshow('Search ball top, ball view', self.top_ball_image)
			cv2.imshow('Search ball bot', self.bot_image)
			cv2.imshow('Search ball bot, ball view',  self.bot_ball_image)

			if self.top_ball is not None:
				print 'Focaldistance top: {}'.format(self.calc_focal_length(self.top_ball[2], distance, object_size))

			if self.bot_ball is not None:
				print 'Focaldistance bot: {}'.format(self.calc_focal_length(self.bot_ball[2], distance, object_size))

			if cv2.waitKey(33) == 27:
				self.shut_down()


	def calc_focal_length(self, radius, distance, object_size):

		pixel_width = radius*2

		return (pixel_width * distance) / object_size


	def distanceTo(self, circle, object_size=5.0):
		pixel_width = circle[2] * 2
		return (object_size * self.focalLength) / pixel_width

	def offlineMode(self, file_path='test_image.png'):

		image = cv2.imread(file_path)
		image, ball_image, ball = self.detect_ball(image)
		cv2.imshow('Main Window', image)
		cv2.waitKey()


	def tune_parameters(self):

		for i in np.arange(10,23):
			image = cv2.imread('original{}.png'.format(i))
			image, ball, _ = self.detect_ball(image)
			cv2.imshow('Original{}'.format(i), image)
			cv2.imshow('Ball{}'.format(i), ball)

		cv2.waitKey()


	def centrate_ball(self, image, ball, threshold = 3):

		if ball is not None:
			center = (image.shape[1] / 2, image.shape[0] / 2)

			center_ball = (int(ball[0]), int(ball[1]))
			diff = np.subtract(center_ball, center)

			distance = np.linalg.norm(np.array(center) - np.array(center_ball))

			cv2.circle(image, center, 5, (0, 0, 255), -1)
			cv2.circle(image, center_ball, 5, (0, 255, 0), -1)
			cv2.arrowedLine(image, center, (center[0] + diff[0], center[1]), (0, 0, 255))
			cv2.arrowedLine(image, center, (center[0], center[1] + diff[1]), (0, 0, 255))
			#TODO check if head at max angles
			target_yaw, target_pitch = self.motion.move_head(diff[0], diff[1])

			if not self.motion.target_in_range(target_yaw, target_pitch):
				return True


			return -threshold < distance < threshold

		return False

	def check_both_cams(self):

		start = time.time()
		self.top_frame = self.topCamera.get_frame(self.topCamera.camera)
		self.bot_frame = self.botCamera.get_frame(self.botCamera.camera)
		end_capture = time.time()
		#print 'Capturing time: {}'.format(end_capture-start)
		#might have to multithread
		self.top_image, self.top_ball_image, self.top_ball = self.detect_ball(self.top_frame)
		self.bot_image, self.bot_ball_image, self.bot_ball = self.detect_ball(self.bot_frame)
		end_processing = time.time()
		#print 'Processing time: {}'.format(end_processing-end_capture)

	def decide_camera(self):

		if self.top_ball is not None and self.bot_ball is None:
			return self.top_ball, self.top_image, self.top_ball_image, True

		elif self.top_ball is None and self.bot_ball is not None:
			return self.bot_ball, self.bot_image, self.bot_ball_image, False

		else:
			# TODO return stuff (biggest ball)
			return self.top_ball, self.top_image, self.top_ball_image, False

	def save_data(self, n_images=3, path=os.path.join('..', 'data', 'raw'), arm='right'):
		n_files = len(os.listdir(path))
		dir_path = os.path.join(path, str(n_files))
		os.mkdir(dir_path)

		arm_angels = np.array(self.motion.get_arm_angles(rightArm=True))
		head_angles = np.array(self.motion.get_head_angles())
		np.save(os.path.join(dir_path, 'right_arm'), arm_angels)
		np.save(os.path.join(dir_path, 'head'), head_angles)

		for i in range(n_images):
			self.check_both_cams()
			top_path = os.path.join(dir_path, 'top{}.png'.format(i))
			bot_path = os.path.join(dir_path, 'bot{}.png'.format(i))
			cv2.imwrite(top_path, self.top_frame)
			cv2.imwrite(bot_path, self.bot_frame)

	def search_vertically(self):
		"""
		Move head downwards to check if ball is also visible with top camera
		(due to the ball finding algorithm it is possible that the ball is seen with the bot camera while
		focusing on the ball with the top camera would be a more logical choice)
		:return:
		"""
		target_pitch = self.motion.limits_head_pitch[1]
		target_yaw, _ = self.motion.get_head_angles()
		self.motion.move_head_to(target_yaw, target_pitch, speed=.1)
		print 'Start vertical search'
		while not self.motion.is_head_on_target(target_yaw, target_pitch):
			print 'Searching vertically...'
			self.top_frame = self.topCamera.get_frame(self.topCamera.camera)
			self.top_image, self.top_ball_image, self.top_ball = self.detect_ball(self.top_frame)
			cv2.imshow('Vertical search', self.top_image)
			cv2.imshow('Vetrical search ball', self.top_ball_image)

			if cv2.waitKey(33) == 27:
				self.shut_down()

			print 'Top ball = '.format(self.top_ball)
			if self.top_ball is not None:
				self.motion.move_head(0, 0) #stop moving
				print 'Not None! :D'
				while True:
					self.top_frame = self.topCamera.get_frame(self.topCamera.camera)
					self.top_image, self.top_ball_image, self.top_ball = self.detect_ball(self.top_frame)
					if self.centrate_ball(self.top_image, self.top_ball):
						self.save_data()
						return
			else:
				self.motion.move_head_to(target_yaw, target_pitch, speed=.1) # might have stopped if tried to centrate, so renew command

	def gather_data(self):
		self.motion.move_head_to(0, 0, speed = 0.6)
		self.motion.cartersian_arm_movement()
		self.motion.setStiffnesses('Head', 1.0)
		turn_wrist = True

		while True:

			#find ball

			if self.search_for_ball():
				print 'Ball found'
				while True: #ball is found, track the ball

					self.check_both_cams()
					if self.top_ball is None and self.bot_ball is None:
						print 'ball lost, looking for ball'
						if not self.lost_ball(): #small search, if the ball is lost briefly
							print 'Ball lost'
							self.search_vertically()
							break #break out the ball is found loop

					ball, image, ball_image, is_top = self.decide_camera()

					if self.centrate_ball(image, ball): #centrate ball, True if ball is centrated
						print 'save data'
						turn_wrist = False

						self.save_data()
						if not is_top:
							self.search_vertically()
						break #start looking for ball again!
						#TODO distance to ball
						#TODO Actually save the data

					cv2.imshow('Main window', image)
					cv2.imshow('Ball view', ball_image)

					if cv2.waitKey(33) == 27:
						self.shut_down()

			if turn_wrist:
				print 'turning wrist'
				self.motion.turn_wrist()
				turn_wrist = False
			elif not turn_wrist:
				turn_wrist= True
				print 'Next arm position'
				self.motion.cartersian_arm_movement()


			#centrate ball
			#save stuff



# for i in range(n_images):


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)


if __name__ == '__main__':
	tracker = BallTracker(sit = True)
	tracker.demo()

	"""
	try:

	except Exception as e:
		PrintException()
		print "stuff died, shutting down"
		print e.message
		tracker.shut_down()
	"""









