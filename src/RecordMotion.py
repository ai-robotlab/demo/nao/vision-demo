
from naoqi import ALBroker
from naoqi import ALProxy
from naoqi import ALModule
import time
import sys

MotionRecorder = None
memory = None

class MotionRecorder(ALModule):

	def __init__(self, ip, port, broker, arm = 'RArm', name = 'MotionRecorder'):
		ALModule.__init__(self, name)
		self.motion = ALProxy('ALMotion', ip, port)
		self.broker = broker
		self.arm = arm

		global memory
		memory = ALProxy('ALMemory')
		memory.subscribeToEvent("FrontTactilTouched", 'MotionRecorder', 'giveJoints')#Event name, mode name, function name
		memory.subscribeToEvent("MiddleTactilTouched", 'MotionRecorder', 'giveJoints')
		memory.subscribeToEvent("RearTactilTouched", 'MotionRecorder', 'giveJoints')


	def giveJoints(self, strVarName, value):

		if value:
			angles = self.motion.getAngles(self.arm, True)
			print "Giving joints for " + self.arm
			print "Shoulder Pitch: {}".format()




if __name__ == '__main__':
	ip = '10.0.1.7'
	port = 9559
	myBroker = ALBroker('myBroker', '0.0.0.0', 0, ip, port)

	global MotionRecorder
	MotionRecorder = MotionRecorder(ip, port, myBroker)


	try:
		while True:
			time.sleep(1)
	except KeyboardInterrupt:
		print
		print "Interrupted by user, shutting down"
		myBroker.shutdown()
		sys.exit(0)