import numpy as np
import cv2

#param
focalLength = 140  # calculated
lower_blue = np.array([70, 60, 40], dtype=np.uint8)
upper_blue = np.array([170, 255, 255], dtype=np.uint8)


def detect_ball(frame):
		"""
		:param image:
		:return image, ball_image, ball:
		"""
		hsvImage = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

		mask = cv2.inRange(hsvImage, lower_blue, upper_blue)

		denoised = remove_noise(mask)  # perform opening and closing

		smoothed = cv2.GaussianBlur(denoised, (13, 13), 0)  # Smooth edges with gaussian blur
		# only pick the ball
		ball_image = cv2.bitwise_and(frame, frame, mask=smoothed)
		gray = ball_image[:, :, 2]

		circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 1, param1=200, param2=20, minRadius=5, maxRadius=100)  # Find circles, hardcoded parameters, are probably better ways of finding these

		image = np.copy(frame)
		if circles is not None:
			circle = circles[0, :][0]

			# draw circle around ball

			cv2.circle(image, (circle[0], circle[1]), circle[2], (0, 255, 0), 2)  # always picking the first seems to work (as it should). Might consider picking the mean of everything.
			ball = circle
		else:
			ball = None

		return image, ball_image, ball


def remove_noise(image):
		kernel = np.ones((9, 9), np.uint8)
		opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
		closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel)

		return closing


def distance_to(circle, object_size=5.0):
	pixel_width = circle[2] * 2
	return (object_size * focalLength) / pixel_width

