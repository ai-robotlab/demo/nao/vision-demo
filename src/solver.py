import numpy as np
import process_data
from tqdm import tqdm
from IPython import embed
import process_data

input_train = np.array(np.load('../data/train_data/input_train_bot.npy'), dtype=np.float32)
input_val = np.array(np.load('../data/train_data/input_val_bot.npy'), dtype=np.float32)
input_test = np.array(np.load('../data/train_data/input_test_bot.npy'), dtype=np.float32)

target_train = np.array(np.load('../data/train_data/cor_target_train_bot.npy'), dtype=np.float32)
target_val = np.array(np.load('../data/train_data/cor_target_val_bot.npy'), dtype=np.float32)
target_test = np.array(np.load('../data/train_data/cor_target_test_bot.npy'), dtype=np.float32)

prototypes = np.array(np.load('../data/train_data/input_train_bot.npy'), dtype=np.float32)
#prototypes = np.array(np.load('../../data/train_data/prototypes/prototypes_bot'+prototypes_type+'.npy'), dtype=np.float32)

def rbf(prototypes, values, beta=0.5):
	activations = np.zeros((values.shape[0], prototypes.shape[0]))
	print 'calculating activations'
	for i, row in tqdm(enumerate(values)):
		result = prototypes - row
		a = np.sqrt(np.sum(np.square(result), axis=1))
		b = -beta * a
		c = np.exp(b)
		# Add normalization
		summed = c.sum()
		activations[i] = c / summed

	print 'Done calculating'
	return activations

def train_weights():
	np.append(input_train, input_val)
	np.append(target_train, target_val)
	train_activations = rbf(prototypes, input_train)
	weights = np.linalg.solve(train_activations, target_train)
	return weights

def evaluate(test_input, test_target, weights):
	activations = rbf(prototypes, test_input)
	predictions = np.dot(activations, weights)
	distance_error = np.linalg.norm(test_target-predictions, axis = 1)
	angle_error = np.linalg.norm(test_target - predictions, axis=0)
	embed()
	return distance_error, angle_error

def find_coor(_input, weights, normalize = True):
	if normalize:
         _input = process_data.standardize_data(_input)
	#TODO normalize
	activation = rbf(prototypes, _input.reshape((1, _input.shape[0])))
	prediction = np.dot(activation, weights)
	return prediction


def pipeline():
	weights = train_weights()
	evaluate(input_test, target_test, weights)

if __name__ == '__main__':
	inp = input_test[0]
	tar = target_test[0]
	weights = train_weights()
	coor = find_coor(inp, weights)
	embed()