import sys
sys.path.append('../')
from lasagne.layers import InputLayer, DenseLayer, batch_norm
import lasagne
from RBFLayer import RBFLayer
import theano.tensor as T
import theano
import numpy as np
import math
import matplotlib.pyplot as plt
from IPython import embed
from tqdm import tqdm
from process_data import create_prototypes

prototypes_type = '20x20'

#Data locations
input_train = np.array(np.load('../../data/train_data/input_train_bot.npy'), dtype=np.float32)
input_val = np.array(np.load('../../data/train_data/input_val_bot.npy'), dtype=np.float32)
input_test = np.array(np.load('../../data/train_data/input_test_bot.npy'), dtype=np.float32)

target_train = np.array(np.load('../../data/train_data/cor_target_train_bot.npy'), dtype=np.float32)
target_val = np.array(np.load('../../data/train_data/cor_target_val_bot.npy'), dtype=np.float32)
target_test = np.array(np.load('../../data/train_data/cor_target_test_bot.npy'), dtype=np.float32)

prototypes = np.array(np.load('../../data/train_data/input_train_bot.npy'), dtype=np.float32)
#prototypes = np.array(np.load('../../data/train_data/prototypes/prototypes_bot'+prototypes_type+'.npy'), dtype=np.float32)

embed()

#network parameters
n_inputs = 4
n_outputs = 3
n_hidden = 150


#Training parameters
alpha = .05
momentum = .1
epochs = 30
n_batches = 10
rbf_beta = 0.5

def create_network(prototypes):
	"""
	Creates the network and training functions
	:param prototypes:
	:return:
	"""

	#Creation of the layers network
	l_in = InputLayer((None, n_inputs))
	l_rbf = RBFLayer(l_in, prototypes, beta=rbf_beta)
	#l_hidden = DenseLayer(l_rbf, num_units=n_hidden)
	#l_hidden2 = DenseLayer(l_hidden, num_units=n_hidden/2, nonlinearity=lasagne.nonlinearities.LeakyRectify())
	#l_hidden3 = DenseLayer(l_hidden2, num_units=n_hidden / 4, nonlinearity=lasagne.nonlinearities.LeakyRectify())
	l_out = DenseLayer(l_rbf, num_units=n_outputs, nonlinearity=lasagne.nonlinearities.LeakyRectify())

	#Trainings functions
	input_var = T.fmatrix()
	target_var = T.fmatrix()
	pred = lasagne.layers.get_output(l_out, inputs=input_var)
	rbf_out = lasagne.layers.get_output(l_rbf, inputs=input_var)
	loss = lasagne.objectives.squared_error(pred, target_var)
	loss = loss.mean()
	params = lasagne.layers.get_all_params(l_out, trainable=True)
	updates = lasagne.updates.sgd(loss, params, learning_rate=alpha)
	train_fn = theano.function([input_var, target_var], [pred, loss, rbf_out], updates=updates)

	val_prediction = lasagne.layers.get_output(l_out, inputs=input_var, deterministic=True)
	val_loss = lasagne.objectives.squared_error(val_prediction, target_var)
	val_loss = val_loss.mean()
	val_fn = theano.function([input_var, target_var], [val_prediction, val_loss, rbf_out])

	return l_out, train_fn, val_fn


def create_batches(input_data, target, n_batches=5):
	dim = input_data.shape
	batch_size = math.trunc(dim[0]/n_batches)
	input_batches = np.split(input_data[:n_batches*batch_size], n_batches)
	target_batches = np.split(target[:n_batches*batch_size], n_batches)

	return input_batches, target_batches

def train_network():

	print 'Creating network...'
	network, train_fn, val_fn = create_network(prototypes)
	print 'Network created.'
	#embed()
	input_batches, target_batches = create_batches(input_train, target_train, n_batches)
	batch_size = target_batches[0].shape[0]
	train_predictions = np.zeros(shape=(batch_size*n_batches, target_batches[0].shape[1]))
	means = np.zeros(shape=(epochs, 3))
	stds = np.zeros(shape=(epochs, 3))
	train_losses = np.zeros(epochs)
	val_losses = np.zeros(epochs)

	val_dists = np.zeros(shape=((epochs,)+target_val.shape))


	print 'Starting training'

	for e in tqdm(range(epochs)):
			total_train_loss = 0
			mean_train_dist = 0
			for i, (input_batch, target_batch) in enumerate(zip(input_batches, target_batches)):
				#embed()
				pred_train, train_loss, rbf_outp = train_fn(input_batch, target_batch)
				total_train_loss += train_loss
				train_predictions[i*batch_size:(i+1)*batch_size] = pred_train #TODO use it
				#embed()
			#embed()
			#Validation testing
			predictions, val_loss, rbf_outp = val_fn(input_val, target_val)
			dist= np.sqrt(np.square(predictions - target_val))

			#Saving results of this epoch
			means[e] = np.mean(dist, axis=0)
			stds[e] = np.std(dist, axis=0)
			train_losses[e] = total_train_loss/n_batches
			val_losses[e] = val_loss
			val_dists[e] = dist


	print 'Done training, printing information...'
	#Plots
	#Plot loss
	plt.figure()
	loss_plot_train, = plt.plot(train_losses, label='Train loss')
	loss_plot_val, = plt.plot(val_losses, label='Validation loss')
	plt.legend(handles=[loss_plot_train, loss_plot_val])
	plt.savefig('results/training'+prototypes_type+'.png')
	plt.show()

	#Plot mean distance
	plt.figure()
	#joints = [arm + 'ShoulderPitch', arm + 'ShoulderRoll', arm + 'ElbowYaw', arm + 'ElbowRoll', arm + 'WristYaw']
	shoulder_pitch, = plt.plot(means[:, 0], label = 'X') #label='Shoulder Pitch')
	shoulder_roll, = plt.plot(means[:, 1],  label = 'Y') #label='Shoulder Roll')
	elbow_yaw, = plt.plot(means[:, 2], label = 'Z') #label='Elbow Yaw')
	#elbow_roll, = plt.plot(means[:, 3], label='Elbow Roll')
	#wrist_yaw, = plt.plot(means[:, 4], label='Wrist Yaw')
	plt.legend(handles=[shoulder_pitch, shoulder_roll, elbow_yaw])#, elbow_roll, wrist_yaw])
	plt.savefig('results/arm_distances'+prototypes_type+'.png')
	plt.show()

	#Plot distance
	plt.figure()
	plt.boxplot(val_dists[-1])
	plt.savefig('results/boxplot'+prototypes_type+'.png')
	plt.show()

	embed()

if __name__ == '__main__':
	train_network()
