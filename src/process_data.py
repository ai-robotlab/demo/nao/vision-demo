import numpy as np
from ball_detector import detect_ball, distance_to
import os
import cv2
import sys
from IPython import embed
import time
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
from tqdm import tqdm

#parameters
raw_path = os.path.join('..', 'data', 'raw')
limits_head_yaw = [-2.0857, 2.0857]
limits_head_pitch = [-0.6720, 0.5149]

"""
Possible prototypes:
yaw_stepsize = 0.2 (4.1714/0.2 = 20)
pitch_stopsize = 0.1 (1.1896/0.1 = 11)
distance = 30
n_camera's = 2
Result = 11*20*30*2 = 13200

yaw_stepsize = 0.4 (4.1714/0.4 = 10)
pitch_stopsize = 0.19 (1.1896/0.19 = 6)
distance = 30
n_camera's = 2
Result = 10*6*30*2 = 3600

yaw_stepsize = 0.4 (4.1714/0.4 = 10)
pitch_stopsize = 0.19 (1.1896/0.19 = 6)
distance = 30/2 = 15
n_camera's = 2
Result = 10*6*15*2 = 1800

"""



def count_images(files):
	i = 0
	for file in files:
		if file.split('.')[-1] == 'png':
			i+=1

	return i


def decide_image(top, bot):

	image, _, top_ball = detect_ball(cv2.imread(top))
	if top_ball is not None:
		return image, top_ball, 'top'

	else:
		image, _, bot_ball = detect_ball(cv2.imread(bot))
		if bot_ball is not None:
			return image, bot_ball, 'bot'

	return None, None, None



def process_raw_data(start_sample = 744):
	samples = os.listdir(raw_path)
	samples.sort(key=int)
	for sample in samples[start_sample:]:

		print 'Processing sample ' + sample
		sample_path = os.path.join(raw_path, sample)

		files = os.listdir(sample_path)
		n_images = count_images(files)

		mean_image, mean_ball, cam_type = decide_image(os.path.join(sample_path, 'top0.png'), os.path.join(sample_path, 'bot0.png'))

		if mean_ball is not None:

			for i in range((n_images/2)-1): #n_images should be an even number
				frame = cv2.imread(os.path.join(sample_path, cam_type + str(i+1) + '.png'))
				image, _, ball = detect_ball(frame)

				if ball is None: #sadly, this sometimes happens, might want to look more into the ball detecting
					break

				mean_ball += ball

			mean_ball = mean_ball/(n_images/2)

			cv2.circle(frame, (mean_ball[0], mean_ball[1]), mean_ball[2], (0, 255, 0), 2)
			cv2.imwrite(os.path.join('../', 'data', 'train_data', 'images', sample + '.png'), image)

		"""
		cv2.imshow('mean image', frame)
		decision = False
		while not decision:
			if cv2.waitKey(33) == ord('y'):
				print 'accepted' + sample
				save_data(frame, mean_ball, sample_path, cam_type)
				decision = True

			elif cv2.waitKey(33) == ord('n'):
				print 'rejected' + sample
				decision = True

			elif cv2.waitKey(33) == 27:
				sys.exit(0)

		cv2.destroyAllWindows()
		"""

def save_data(image, ball, sample_path, cam_type):
	"""
	Save format:
	Input: head_yaw, head_pitch, distance_to_ball, cam_type
	:param mean_ball:
	:param sample_path:
	:return:
	"""
	sample_id = sample_path.split('\\')[-1]
	input_ = np.zeros(4)
	input_[0:2] = np.load(os.path.join(sample_path, 'head.npy'))
	input_[2] = distance_to(ball)
	if cam_type == 'top':
		input_[3] = 0
	else:
		input_[3] = 1

	np.save(os.path.join('../', 'data', 'train_data', 'input', sample_id + '.npy'), input_)
	cv2.imwrite(os.path.join('../', 'data', 'train_data', 'images', sample_id + '.png'), image)


def combine_data(file_path = os.path.join('../', 'data', 'train_data'), input_path = os.path.join('../', 'data', 'train_data', 'bot_input.npy'), target_path = os.path.join('../', 'data', 'train_data', 'bot_target.npy')):

	included_path = os.path.join(file_path, 'images')
	images = os.listdir(included_path)
	_input = np.empty((0, 3))
	target = np.empty((0, 5))
	coor_target = np.empty((0, 3))

	for sample in tqdm(images):
		sample_path = os.path.join(raw_path, sample.split('.')[0])
		image, ball, cam_type = decide_image(os.path.join(sample_path, 'top0.png'), os.path.join(sample_path, 'bot0.png'))
		if cam_type == 'bot':
			#print sample

			distance = distance_to(ball)
			input_sample = np.load(os.path.join(sample_path, 'head.npy'))
			input_sample = np.array([np.append(input_sample, distance)])
			target_sample = np.array([np.load(os.path.join(sample_path, 'right_arm.npy'))])
			coor_sample = np.array([np.load((os.path.join(sample_path, 'pos.npy')))])
			_input = np.append(_input, input_sample, axis=0)
			target = np.append(target, target_sample, axis=0)
			coor_target = np.append(coor_target, coor_sample, axis=0)

	np.save(input_path, _input)
	np.save(target_path, target)
	np.save(os.path.join('../', 'data', 'train_data', 'bot_coor_target.npy'), coor_target )


def split_data(input_data, target_data, val_size = 0.1, test_size =  0.1, out_path = os.path.join('../', 'data', 'train_data')):
	dim = input_data.shape
	n_val = math.trunc(dim[0]*val_size)
	n_test = math.trunc(dim[0]*val_size)
	n_train = dim[0]-n_test-n_val

	#shuffle
	#indices = np.arange(len(input_data))
	#np.random.shuffle(indices)
	#input_data = input_data[indices]
	#target_data = target_data[indices]

	#save input data
	input_val_set, input_test_set, input_train_set = np.split(input_data, (n_val, n_val+n_test))

	np.save(os.path.join(out_path, 'input_val_bot.npy'), input_val_set)
	np.save(os.path.join(out_path, 'input_test_bot.npy'), input_test_set)
	np.save(os.path.join(out_path, 'input_train_bot.npy'), input_train_set)

	#Save target data
	target_val_set, target_test_set, target_train_set = np.split(target_data, (n_val, n_val + n_test))

	np.save(os.path.join(out_path, 'cor_target_val_bot.npy'), target_val_set)
	np.save(os.path.join(out_path, 'cor_target_test_bot.npy'), target_test_set)
	np.save(os.path.join(out_path, 'cor_target_train_bot.npy'), target_train_set)

def standardize_data(data, stat_file_name = 'input_statistics.npy'):
	if os.path.isfile(stat_file_name):
		statistics = np.load(stat_file_name)
		mean = statistics[0]
		std = statistics[1]

	else:
		mean = data.mean(axis=0)
		std = data.std(axis=0)
		np.save(stat_file_name, np.array([mean, std]))

	return (data-mean)/std




def create_prototypes(data, n_head_proto=20, n_distance_proto=20):

	kmeans_head = KMeans(n_clusters=n_head_proto, verbose=0)
	kmeans_head.fit(data[:, :2])
	head_prototypes = kmeans_head.cluster_centers_
	kmeans_distance = KMeans(n_clusters=n_distance_proto, verbose=0)
	distance_data = data[:, -1].reshape((len(data[:, -1]), 1))
	kmeans_distance.fit(distance_data)

	distance_proto = kmeans_distance.cluster_centers_.flatten()
	distance_proto = np.sort(distance_proto)

	prototypes = np.zeros((n_head_proto*n_distance_proto, head_prototypes.shape[1] + 1))
	for i, proto in enumerate(distance_proto):
		dist = np.full((n_head_proto,1), proto)
		proto_group = np.hstack((head_prototypes, dist))

		prototypes[i*n_head_proto:(i+1)*n_head_proto] = proto_group

	np.save('../data/train_data/prototypes/prototypes_bot{}x{}.npy'.format(n_head_proto, n_distance_proto), prototypes)

	"""
	print distance_proto
	ax = fig.add_subplot(111, projection = '3d')
	ax.scatter(prototypes[:, 0], prototypes[:, 1], prototypes[:, 2])
	ax.set_xlabel('Yaw')
	ax.set_ylabel('Pitch')
	ax.set_zlabel('Distance')
	plt.show()
	"""


def pipeline():
	combine_data()
	input_data = np.load('../data/train_data/bot_input.npy')
	target_data = np.load('../data/train_data/bot_coor_target.npy')
	input_data = standardize_data(input_data, 'input_statistics.npy')
	create_prototypes(input_data)
	split_data(input_data, target_data)

if __name__ == '__main__':
	pipeline()
	#target_data = np.load('../data/train_data/bot_target.npy')

	#create_prototypes(input_data)

