from naoqi import ALProxy
import random
import numpy as np
import time
import sys
import os
import almath
import motion
from math import cos, acos
from tqdm import tqdm
import NAOCamera
import cv2
from IPython import embed


class NAOMovement:

	def __init__(self, ip, port, sit = False):
		self.motion = ALProxy('ALMotion', ip, port)

		robotConfig = self.motion.getRobotConfig()
		self.robotName = ""
		for i in range(len(robotConfig[0])):
			if (robotConfig[0][i] == 'Model Type'):
				self.robotName = robotConfig[1][i]

		succes = self.motion.setCollisionProtectionEnabled('Arms', True)
		if not succes:
			print 'Failed to init collision protection, be careful!'

		self.limits_head_yaw = [-2.0857, 2.0857]
 		self.limits_head_pitch = [-0.6720, 0.5149]

		self.limits_left = [(-2.0857, 2.0857), (-0.3142, 1.3265), (-2.0857, 2.0857), (-1.5446, -0.0349), (-1.8238, 1.8238)]#ShoulderPitch, ShoulderRoll, ElbowYaw, ElbowRoll, WristYaw
		self.limits_right = [(-2.0857, 2.0857), (-1.3265, 0.3142), (-2.0857, 2.0857), (0.0349, 1.5446), (-1.8238, 1.8238)]

		self.prev_head_yaw, self.prev_head_pitch = self.get_head_angles()


		self.memory = ALProxy("ALMemory", ip, port)
		self.sensorsHead = [
			'Device/SubDeviceList/Head/Touch/Front/Sensor/Value',
			'Device/SubDeviceList/Head/Touch/Rear/Sensor/Value',
			'Device/SubDeviceList/Head/Touch/Middle/Sensor/Value'
		]

		self.sensorsFeet = [

		]
		self.posture = ALProxy('ALRobotPosture', ip, port)
		if sit:
			self.sit()




	def move_arm(self, joints, angles, rightArm):

		if rightArm:
			arm = 'R'
		else:
			arm = 'L'

		self.motion.setStiffnesses(arm + 'Arm', 1.0)
		self.motion.setAngles(joints, angles, 0.2)



	def random_arm_movement(self, rightArm = True):
		if rightArm:
			limits = self.limits_right
		else:
			limits = self.limits_left

		joints = self.get_jointslist(rightArm)
		angles = [random.uniform(lower, upper) for (lower, upper) in limits]


		self.move_arm(joints, angles, rightArm)


	def sit(self, rest = False):
		self.posture.goToPosture('Stand', 1.0)
		if rest:
			self.motion.rest()


	def grab_stick(self, hand = 'RHand'):

		self.motion.openHand(hand)
		#self.motion.setStiffnesses('RHand', 1.0)
		close = False
		while not close:
			for sensor in self.sensorsHead:
				if self.memory.getData(sensor):
					print "closing hand"
					self.motion.closeHand(hand)
					close = True
					self.motion.setStiffnesses(hand, 1.0)
		return


	def get_jointslist(self, rightArm):

		if rightArm:
			arm = 'R'
		else:
			arm = 'L'

		joints = [arm + 'ShoulderPitch', arm + 'ShoulderRoll', arm + 'ElbowYaw', arm + 'ElbowRoll', arm + 'WristYaw']

		return joints

	def monitor_cartesian(self, rightArm = True):

		joints = self.get_jointslist(rightArm)

		if rightArm:
			arm = 'RArm'
		else:
			arm = 'LArm'

		transform = almath.Transform(self.motion.getTransform(arm, motion.FRAME_TORSO, False))
		pos = almath.position3DFromTransform(transform)
		output = "\r"
		output += str(pos)

		os.system('cls')
		sys.stdout.write(output)
		sys.stdout.flush()

	def monitor_joints(self, rightArm = True):

		joints = self.get_jointslist(rightArm)


		if rightArm:
			arm = 'right'
		else:
			arm = 'left'

		angles = self.motion.getAngles(joints, True)
		output = ""
		for i, angle in enumerate(angles):
				output += '\r\n{} \t\t {}'.format(joints[i], angle)

		os.system('cls')
		sys.stdout.write(output)
		sys.stdout.flush()

	def is_head_on_target(self, yaw_target, pitch_target, yaw_threshold=0.25, pitch_treshold=0.15):
		"""
		Checks if head is on target. Currently only checks yaw
		:param yaw_target:
		:param pitch_target:
		:param yaw_threshold:
		:param pitch_treshold:
		:return:
		"""
		current_yaw, current_pitch = self.get_head_angles()

		yaw_distance = (current_yaw-self.limits_head_yaw[0])**2 - (yaw_target-self.limits_head_yaw[0])**2
		pitch_limit = self.limits_head_pitch[0] if pitch_target < 0 else self.limits_head_pitch[1]
		pitch_distance = (current_pitch - pitch_limit)**2 - (pitch_target - pitch_limit)**2
		#print 'pitch {}'.format(pitch_distance)
		#print 'yaw {}'.format(yaw_distance)

		if -yaw_threshold < yaw_distance < yaw_threshold and -pitch_treshold < pitch_distance < pitch_treshold:
			return True
		else:
			return False

	def target_in_range(self, target_yaw, target_pitch):

		return self.limits_head_yaw[0] < target_yaw < self.limits_head_yaw[1] and \
			   self.limits_head_pitch[0] < target_pitch < self.limits_head_pitch[1]

	def setStiffnesses(self, name, value):
		self.motion.setStiffnesses(name, value)

	def move_head_to(self, yaw, pitch, speed = 0.10):
		"""
		Moving the head to a given position
		:param yaw:
		:param pitch:
		:param speed:
		:return:
		"""
		self.motion.setStiffnesses('Head', 0.8)
		names = ['HeadYaw', "HeadPitch"]
		angles = [yaw, pitch]
		self.motion.setAngles(names, angles, speed)

	def turn_wrist(self, wrist = 'RWristYaw'):
		current_angle = self.motion.getAngles([wrist], True)[0]
		self.motion.setAngles([wrist], [acos(-cos(current_angle))], 0.5)



	def cartesian_move_arm_to(self, coor):
		if self.is_valid_pos(coor):
			print 'Move arm'
			effector = 'RArm'
			frame = motion.FRAME_TORSO
			axisMask = almath.AXIS_MASK_VEL
			pos = almath.Position3D(coor[0], coor[1], coor[2])
			transform = almath.transformFromPosition3D(pos)
			path = []
			path.append(list(transform.toVector()))
	
			times = [5.0]
			self.motion.closeHand('RHand')
			self.motion.transformInterpolations(effector, frame, path, axisMask, times)
			self.motion.openHand('RHand')
			return True
			
		else:
			return False					
				
	def cartersian_arm_movement(self):

		self.motion.openHand('RHand')
		effector = "RArm"
		frame = motion.FRAME_TORSO
		print frame
		axisMask = almath.AXIS_MASK_VEL
		useSensorValues = False


		path = []
		currentTf = self.motion.getTransform(effector, frame, useSensorValues)
		x = random.uniform(0, 0.2)
		y = random.uniform(-.3, .3)
		z = random.uniform(-.15, .3)
		targetPos = almath.Position3D(x, y, z)
		print targetPos
		targetTf = almath.transformFromPosition3D(targetPos)
		path.append(list(targetTf.toVector()))


		# Go to the target and back again
		times = [5.0]  # seconds

		self.motion.transformInterpolations(effector, frame, path, axisMask, times)
		self.motion.setStiffnesses(effector, 0.6)
		self.motion.openHand('RHand')

		"""
		path = []
		currentTf = almath.Transform(self.motion.getTransform(effector, frame, useSensorValues))
		targetPos = almath.Position3D(.007, -.17, .216)
		targetTf = almath.transformFromPosition3D(targetPos).toVector()
		self.motion.transformInterpolation([effector], frame, targetTf, [axisMask], [1.5])
		"""

	def get_head_angles(self):
		"""

		:return: head yaw, head pitch
		"""
		return self.motion.getAngles(['HeadYaw', 'HeadPitch'], 1)


	def get_arm_angles(self, rightArm = True):
		"""
		:param right_arm: True if right, False is left
		:return: arm angles in radians
		"""
		joints = self.get_jointslist(rightArm)
		return self.motion.getAngles(joints, 1)

	def is_valid_pos(self, pos):
		x_lim = (0, 0.2)
		y_lim = (-0.3, 0.3)
		z_lim = (-0.15, 0.3)
		
		if x_lim[0] <= pos[0] <= x_lim[1] and y_lim[0] <= pos[1] <= y_lim[1] and z_lim[0] <= pos[2] <= z_lim[1]:
			return True
		else:
			print 'Position out of range'
			return False


	def move_head(self, diff_x, diff_y, factor=0.001):
		"""

		:param diff_x:
		:param diff_y:
		:param factor:
		:return: target_yaw, target_pitch
		"""
		self.motion.setStiffnesses("Head", 0.8)
		names = ['HeadYaw', "HeadPitch"]
		changes = [-diff_x*factor, diff_y*factor]

		maxSpeed = 0.03
		curr_head_yaw, curr_head_pitch = self.get_head_angles()
		target_yaw = curr_head_yaw + changes[0]
		target_pitch = curr_head_pitch + changes[1]
		self.motion.changeAngles(names, changes, maxSpeed)

		return target_yaw, target_pitch

	def repeat_movements(self, file_path = os.path.join('../', 'data', 'raw')):
		files = os.listdir(file_path)
		joints = self.get_jointslist(True)

		for file_ in tqdm(files):

			arm_angles = np.load(os.path.join(file_path, file_, 'right_arm.npy'))
			self.motion.angleInterpolation(joints, list(arm_angles), [1, 1, 1, 1, 1], True)
			time.sleep(1)
			position6d = almath.Position6D(self.motion.getPosition('RHand', motion.FRAME_TORSO, True))
			position3d = almath.position3DFromPosition6D(position6d)
			#np.save(os.path.join(file_path, file_, 'pos.npy'), position3d.toVector())


if __name__ == '__main__':
	movement = NAOMovement(ip = '10.0.1.2', port = 9559, sit = True)
	movement.cartesian_move_arm_to(np.array([0.1,0.2,0.1]))








