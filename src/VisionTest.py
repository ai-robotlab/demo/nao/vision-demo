# -*- coding: utf-8 -*-
"""
Created on Thu Mar 10 15:04:49 2016

@author: The Mountain
"""

from naoqi import ALProxy
import numpy as np
import NAOCamera
import cv2
import time
from IPython import embed

FOCAL_LENGTH = 240
IP="131.174.106.223"
PORT=9559
motion = ALProxy("ALMotion", IP, PORT)
# Thresholds for ball
LOWER_BLUE = np.array([90, 80, 80], dtype=np.uint8)
UPPER_BLUE = np.array([140, 255, 255], dtype=np.uint8)



def detect_ball(image):
	hsvImage = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

	mask = cv2.inRange(hsvImage, LOWER_BLUE, UPPER_BLUE)

	denoised = remove_noise(mask)  # perform opening and closing

	smoothed = cv2.GaussianBlur(denoised, (3, 3), 0)  # Smooth edges with gaussian blur
	# only pick the ball
	ball_image = cv2.bitwise_and(image, image, mask=smoothed)
	gray = ball_image[:, :, 2]


	circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 1, param1=200, param2=20, minRadius=0,
							   maxRadius=100)  # Find circles, hardcoded parameters, are probably better ways of finding these

	if circles is not None:
		circle = circles[0, :][0]

		# draw circle around ball
		cv2.circle(image, (circle[0], circle[1]), circle[2], (0, 255, 0), 2)  # always picking the first seems to work (as it should). Might consider picking the mean of everything.

		# calc distance to ball
		distance = distanceTo(circle)
		distance_text = format(distance, '0.2f')

		cv2.putText(image, distance_text, (int(circle[0] + circle[2]) + 5, int(circle[1] + circle[2] + 5)),
					cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
		ball = circle
	else:
		ball = None

	return image, ball_image, ball



def distance_to_ball(nao_cam):
	nao_cam.subscribe_camera()

	while (True):
		image = nao_cam.get_frame(nao_cam.camera)
		image, ball_image, ball = detect_ball(image)  # ball_image is for debugging purposes
		image = centrate_ball(image, ball)
		cv2.imshow("Distance to ball", image)
		cv2.imshow("Balls cam", ball_image)

		# exit by [ESC]
		if cv2.waitKey(33) == 27:
			nao_cam.unsubscribe_camera()
			cv2.destroyWindow("Distance to ball")
			break


def remove_noise(image):
	kernel = np.ones((5, 5), np.uint8)
	opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
	closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel)

	return closing


"""
circle object
distance to object in cm
diameter of object in cm
"""
def calc_focal_length(circle, distance=30, object_size=7.0):
	pixel_width = circle[2] * 2  # 2*the radius of the circle

	return (pixel_width * distance) / object_size


def distanceTo(circle, object_size=7.0):
	pixel_width = circle[2] * 2
	return (object_size * FOCAL_LENGTH) / pixel_width


def offlineMode(file_path='test_image.png'):

	image = cv2.imread(file_path)
	image, ball_image, ball = detect_ball(image)
	cv2.imshow('Main Window', image)
	cv2.waitKey()


def centrate_ball(image, ball):


	if ball is not None:
		center  = (image.shape[1]/2, image.shape[0]/2)
		#embed()

		center_ball = (int(ball[0]), int(ball[1]))
		diff = np.subtract(center_ball, center)
		print center_ball
		print diff

		cv2.circle(image, center, 5,  (0, 0, 255), -1)
		cv2.circle(image, center_ball, 5, (0, 255, 0), -1)
		cv2.arrowedLine(image, center, (center[0] + diff[0], center[1]), (0, 0, 255))
		cv2.arrowedLine(image, center, (center[0], center[1] + diff[1]), (0, 0, 255))

	return image


def move_head(diff_x, diff_y, speed = 5):

	motion.setStiffnesses("Head", 0.8)
	names = ['HeadYaw', "HeadPitch"]
	changes = 0.5, 0.5
	maxSpeed = 0.05

	print motion.changeAngles(names, changes, maxSpeed)
	time.sleep(2.0)
	motion.setStiffnesses("Head", 0.0)





if __name__ == "__main__":
	move_head(0,0,0)
	#nao_cam = NAOCamera.NaoCam()
	#distance_to_ball(nao_cam)
	#offlineMode()
