from naoqi import ALProxy
import cv2
import numpy as np
from IPython import embed

resolutions = {
	'kQQQQVGA' : 8,#40x30
	'kQQQVGA' : 7,#80x60
	'kQQVGA' : 0,#160x120
	'kQVGA' : 1,#320*240
	'kVGA' : 2,#640x480
	'k4VGA' : 3,#1280x960
}

class NaoCam:

	def __init__(self, name = 'cam', IP="131.174.106.223", PORT=9559, bottom_camera = 1):
		self.IP = IP
		self.PORT = PORT
		# get NAOqi module proxy
		self.video = ALProxy('ALVideoDevice', self.IP, self.PORT)
		self.cam_type = bottom_camera
		self.name = name
		self.width = 320# kan dynamisch, zit in de header van de image
		self.height = 240



	def get_frame(self, camera):
		# create image
		result = self.video.getImageRemote(camera)
		width = result[0]
		height = result[1]
		image = np.zeros((height, width, 3), np.uint8)
		if result == None:
			print 'cannot capture.'
		elif result[6] == None:
			print 'no image data string.'
		else:
			# translate value to mat
			values = map(ord, list(result[6]))
			i = 0
			for y in range(0, height):
				for x in range(0, width):
					image.itemset((y, x, 0), values[i + 0])
					image.itemset((y, x, 1), values[i + 1])
					image.itemset((y, x, 2), values[i + 2])
					i += 3

			return image

	def subscribe_camera(self):

		AL_kBGRColorSpace = 13
		self.camera = self.video.subscribeCamera(self.name, self.cam_type, resolutions['kQQVGA'], AL_kBGRColorSpace, 10)
		print self.camera

	def unsubscribe_camera(self):
		if self.camera is not None:
			print "unsubscribing..." + self.camera
			self.video.unsubscribe(self.camera)

	def unsubscribe_all_cameras(self):
		cams = self.video.getSubscribers()
		for cam in cams:
			self.video.unsubscribe(cam)

	def film(self):

		AL_kBGRColorSpace = 13
		camera = self.video.subscribeCamera("cam_luc", self.cam_type, resolutions['kQVGA'], AL_kBGRColorSpace, 10)

		while True:
			image = self.get_frame(camera)
			# show image
			cv2.imshow("nao-top-camera-320x240", image)


			# exit by [ESC]
			if cv2.waitKey(33) == 27:
				cv2.destroyWindow("nao-top-camera-320x240")
				break

		self.video.unsubscribe(camera)


		return image


if __name__ == '__main__':
	cam = NaoCam(IP = '10.0.1.2')
	cam.unsubscribe_all_cameras()

